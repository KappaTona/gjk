%module GJK

%{
#include <gjk_swig_wrapper.h>
%}

%include <std_shared_ptr.i>

%shared_ptr(gjk_swig_wrapper);

%include <gjk_swig_wrapper.h>
