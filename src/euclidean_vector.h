#ifndef EUCLIDEAN_VECTOR_H__
#define EUCLIDEAN_VECTOR_H__
#include "point.hpp"
#include <ostream>

namespace geometry {

class EuclideanVector {
    yx_t vec_;

public:
    EuclideanVector();
    EuclideanVector(yx_t vec);
    EuclideanVector(double x, double y);
    EuclideanVector(const EuclideanVector& vec);
    EuclideanVector(EuclideanVector&& vec);

    EuclideanVector& operator-(EuclideanVector other);
    EuclideanVector& operator=(const EuclideanVector& other);
    EuclideanVector operator*(double scalar) const;
    EuclideanVector& operator*=(double scalar);
    EuclideanVector& operator+=(const EuclideanVector& other);
    EuclideanVector& operator-=(const EuclideanVector& other);
    EuclideanVector& operator/=(double scalar);
    EuclideanVector& perpendicular();
    EuclideanVector& T();

    double dot(const EuclideanVector& other) const;
    double size() const;
    double length_squared() const;
    double x() const;
    double y() const;

    void x(double x);
    void y(double y);

    friend bool operator==(const EuclideanVector& one, const EuclideanVector& other);
    friend bool operator!=(const EuclideanVector& one, const EuclideanVector& other);
    friend bool operator<(const EuclideanVector& one, const EuclideanVector& other);
    friend bool operator>(const EuclideanVector& one, const EuclideanVector& other);
    friend bool operator<=(const EuclideanVector& one, const EuclideanVector& other);
    friend bool operator>=(const EuclideanVector& one, const EuclideanVector& other);
};

EuclideanVector operator+(const EuclideanVector& one, const EuclideanVector& other);
EuclideanVector operator-(const EuclideanVector& one, const EuclideanVector& other);
EuclideanVector operator!(const EuclideanVector& one);
EuclideanVector triple_dot(const EuclideanVector& left, const EuclideanVector& mid, const EuclideanVector& right);




}

std::ostream& operator<<(std::ostream& os, const geometry::EuclideanVector& p);


#endif
