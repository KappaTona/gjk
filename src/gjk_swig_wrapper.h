#ifndef GJK_SWIG_WRAPPER_H__
#define GJK_SWIG_WRAPPER_H__
#include <memory>


class gjk_swig_wrapper {
public:
    virtual ~gjk_swig_wrapper();
    static std::shared_ptr<gjk_swig_wrapper> create();

    virtual bool graze_with_one_dimensions(double y1, double x1, double y2, double x2) const =0;
    virtual bool collision_with_one_dimensions(double y1, double x1, double y2, double x2) const =0;
};

#endif //GJK_SWIG_WRAPPER_H__
