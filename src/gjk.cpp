#include "gjk.h"
#include "shape_1d.h"
#include "euclidean_vector.h"
#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <tuple>
#include <utility>
#include <vector>
#include <set>


namespace {
using namespace geometry;
using EuclideanPolygon_t = std::vector<EuclideanVector>;

EuclideanVector avarge_point(const EuclideanPolygon_t& verticies)
{
    EuclideanVector avg;
    for (auto& vec: verticies)
        avg += vec;
    
    avg /= verticies.size();
    return avg;
}

size_t index_of_furtherest_point(const EuclideanPolygon_t& verticies, const EuclideanVector& point)
{
    std::set<std::pair<double, int>> dot_products_indexed;
    int i = 0;
    for(const auto& y: verticies)
        dot_products_indexed.emplace(std::make_pair(point.dot(y), i++));

    return dot_products_indexed.crbegin()->second;
}


EuclideanVector support(const EuclideanPolygon_t& vec1, const EuclideanPolygon_t& vec2, EuclideanVector const& point)
{
    size_t i = index_of_furtherest_point(vec1, point);
    size_t j = index_of_furtherest_point(vec2, !point);
    return vec1[i] - vec2[j];
}


}

namespace algorithm {

gjk::gjk()
{
}

verbose_bool_t gjk::boom(Shape1D one, Shape1D another) const
{
    if (collision(one, another))
        return {true, "collision"};

    if (graze(one, another))
        return {true, "graze"};

    return {false, "no boom"};
}

bool gjk::collision(Shape1D one, Shape1D another) const
{
    auto [o_begin, o_end] = one.bounds();
    auto [a_begin, a_end] = another.bounds();
    return o_begin - a_end < 0.
        && o_end - a_begin > 0.;
}

bool gjk::graze(Shape1D one, Shape1D another) const
{
    auto [o_begin, o_end] = one.bounds();
    auto [a_begin, a_end] = another.bounds();
    return o_begin - a_end == 0.
        || o_end - a_begin == 0.;
}



bool gjk::collision(const EuclideanPolygon_t& left, const EuclideanPolygon_t& right) const
{
    if (!is_convex(left) || !is_convex(right))
        throw std::runtime_error("One or more EuclideanPolygon are not convex. gjk::collision terminates.");

    size_t index = 0;
    EuclideanPolygon_t simplexes{3};
    auto position_left = avarge_point(left);
    auto position_right = avarge_point(right);
    auto direction = position_left - position_right;

    // if direction is zero
    if (direction.length_squared() == 0)
        direction.x(1.);

    // set first support as initial point of the new simplex
    auto a = support(left, right, direction);
    simplexes[0] = a;

    if (a.dot(direction) <= 0.)
        return false;

    // search towards the origin
    direction = !a;
    EuclideanVector ab, ac, acperp, abperp;
    while (true)
    {
        a = simplexes[++index] = support(left, right, direction);

        // itt
        if (a.dot(direction) <= 0.)
            return false;

        auto ao = !a;

        if (index < 2)
        {
            ab = simplexes[0] - a;
            direction = triple_dot(ab, ao, ab);

            if (!direction.length_squared())
                direction = ab.perpendicular();

            continue; // skip to next iteration
        }

        ab = simplexes[1] - a;
        ac = simplexes[0] - a;

        acperp = triple_dot(ab, ac, ac);

        if (acperp.dot(ao) >= 0)
        {
            direction = acperp;
        }
        else
        {
            abperp = triple_dot(ac, ab, ab);
            if (abperp.dot(ao) < 0)
                return true;

            simplexes[0] = simplexes[1];
            direction = abperp;
        }

        simplexes[1] = simplexes[2];
        --index;
    }

    return true;
}

bool gjk::is_convex(const EuclideanPolygon_t& polygon) const
{
//  source: https://math.stackexchange.com/questions/1743995/determine-whether-a-polygon-is-convex-based-on-its-vertices

    if (polygon.size() < 3)
        return false;

    auto sign_changed = [](auto& sign_flips, bool changed)
    {
        auto& [sign, flips] = sign_flips;
        if (changed)
        {
            ++flips;
            sign *= -1;
        }
    };

    auto& curr = *(polygon.cend() - 2);
    auto& next = *(polygon.cend() - 1);
    auto sign_diff = next - curr;
    int y_first_sign = sign_diff.y() > 0 ? 1 : -1;
    int x_first_sign = sign_diff.x() > 0 ? 1 : -1;
    double w_sign = 0;
    std::pair<int, int> x_sign_flips = {x_first_sign, -1};
    std::pair<int, int> y_sign_flips = {y_first_sign, -1};
    

    // Each vertex in order
    for (auto& v: polygon)
    {
        auto& prev = curr;
        auto& curr = next;
        auto& next = v;

        auto next_edge = next - curr;

        sign_changed(y_sign_flips, std::signbit(next_edge.y()) != std::signbit(y_sign_flips.first));
        sign_changed(x_sign_flips, std::signbit(next_edge.x()) != std::signbit(x_sign_flips.first));

        if (y_sign_flips.second > 2 || x_sign_flips.second > 2)
            return false;

        // Find out the orientation of this pair of edges, and ensure it does not differ from perv ones.
        auto prev_edge = curr - prev;
        double w = prev_edge.y() * next_edge.x() - next_edge.y() * prev_edge.x();
        if (w_sign == 0 && w != 0)
            w_sign = w;

        if ((w_sign > 0 && w < 0) || (w_sign < 0 && w > 0))
           return false;
    }

    auto final_wraparound_sign_flips = [](auto& sign_flips, int first_sign) {
        auto& [sign, flips] = sign_flips;
        if (sign != first_sign)
            ++flips;
    };
    final_wraparound_sign_flips(x_sign_flips, x_first_sign);
    final_wraparound_sign_flips(y_sign_flips, y_first_sign);


    // Concave polygons have two sign flips along each axis.
    if (x_sign_flips.second == 2 && y_sign_flips.second == 2)
        return false;

    return true;
}


}
