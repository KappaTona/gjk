#ifndef GJK_H__
#define GJK_H__
#include <utility>
#include <vector>
namespace geometry {
class Shape1D;
class EuclideanVector;
}

namespace algorithm {
using namespace geometry;
using verbose_bool_t = std::pair<bool, const char*>;


class gjk {
public:
    gjk();

    verbose_bool_t boom(Shape1D one, Shape1D another) const;
    bool graze(Shape1D one, Shape1D another) const;
    bool collision(Shape1D one, Shape1D another) const;

    bool collision(const std::vector<EuclideanVector>& left, const std::vector<EuclideanVector>& right) const;
    bool is_convex(const std::vector<geometry::EuclideanVector>& ordered_polygon) const;

};


}
#endif
