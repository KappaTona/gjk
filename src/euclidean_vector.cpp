#include "euclidean_vector.h"
#include <iterator>
#include <tuple>


namespace geometry {
EuclideanVector::EuclideanVector(): vec_{0.,0.}
{
}

EuclideanVector::EuclideanVector(yx_t vec): vec_{vec}
{
}

EuclideanVector::EuclideanVector(double y, double x): EuclideanVector{{y, x}}
{
}

EuclideanVector::EuclideanVector(EuclideanVector&& vec)
    : vec_{std::move(vec.vec_)}
{
}

EuclideanVector::EuclideanVector(const EuclideanVector& vec)
    : vec_{vec.vec_}
{
}

double EuclideanVector::y() const
{
    return vec_.first;
}

double EuclideanVector::x() const
{
    return vec_.second;
}

EuclideanVector& EuclideanVector::operator-(EuclideanVector other)
{
    vec_.first -= other.y();
    vec_.second -= other.x();
    return *this;
}


EuclideanVector operator!(const EuclideanVector& one)
{
    return {one.y() * -1, one.x() * -1};
}

EuclideanVector& EuclideanVector::perpendicular()
{
    vec_.second *= -1;
    std::swap(vec_.first, vec_.second);
    return *this;
}

EuclideanVector& EuclideanVector::T()
{
    return perpendicular();
}

double EuclideanVector::dot(const EuclideanVector& other) const
{
    return y() * other.y() + x() * other.x();
}



double EuclideanVector::size() const
{
    return y() * y() + x() * x();
}

double EuclideanVector::length_squared() const
{
    return size();
}

EuclideanVector EuclideanVector::operator*(double scalar) const
{
    return {y() * scalar, x() * scalar};
}

EuclideanVector& EuclideanVector::operator*=(double scalar)
{
    vec_.first *= scalar;
    vec_.second *= scalar;
    return *this;
}

EuclideanVector operator+(const EuclideanVector& one, const EuclideanVector& another)
{
    auto x = one.x() + another.x();
    auto y = one.y() + another.y();
    return {{y, x}};
}

EuclideanVector operator-(const EuclideanVector& one, const EuclideanVector& another)
{
    auto x = one.x() - another.x();
    auto y = one.y() - another.y();
    return {{y, x}};
}

EuclideanVector& EuclideanVector::operator-=(const EuclideanVector& other)
{
    vec_.first -= other.y();
    vec_.second -= other.x();
    return *this;
}

EuclideanVector& EuclideanVector::operator=(const EuclideanVector& other)
{
    vec_.first = other.y();
    vec_.second = other.x();
    return *this;
}

EuclideanVector& EuclideanVector::operator+=(const EuclideanVector& other)
{
    vec_.first += other.vec_.first;
    vec_.second += other.vec_.second;
    return *this;
}

EuclideanVector& EuclideanVector::operator/=(double scalar)
{
    vec_.first /= scalar;
    vec_.second /= scalar;
    return *this;
}

bool operator==(const EuclideanVector& one, const EuclideanVector& other)
{
    return std::tie(one.vec_.first, one.vec_.second)
        == std::tie(other.vec_.first, other.vec_.second);
}

bool operator!=(const EuclideanVector& one, const EuclideanVector& other)
{
    return !(one == other);
}

bool operator<(const EuclideanVector& one, const EuclideanVector& other)
{
    return std::tie(one.vec_.first, one.vec_.second)
        < std::tie(other.vec_.first, other.vec_.second);
}

bool operator>(const EuclideanVector& one, const EuclideanVector& other)
{
    return other < one;
}

bool operator>=(const EuclideanVector& one, const EuclideanVector& other)
{
    return !(one < other);
}

bool operator<=(const EuclideanVector& one, const EuclideanVector& other)
{
    return !(one > other);
}



void EuclideanVector::x(double x)
{
    vec_.second = x;
}

void EuclideanVector::y(double y)
{
    vec_.first = y;
}

EuclideanVector triple_dot(const EuclideanVector& left, const EuclideanVector& mid, const EuclideanVector& right)
{
    return {(mid * left.dot(right)) - (left * mid.dot(right))};;
}

}


std::ostream& operator<<(std::ostream& os, const geometry::EuclideanVector& p)
{
    os << "y: " << p.y() << ", x: " << p.x() << std::endl;
    return os;
}
