#ifndef GJK_CONNECTOR_H__
#define GJK_CONNECTOR_H__
#include "gjk_swig_wrapper.h"
#include <memory>
namespace algorithm {
class gjk;
}


class gjk_connector : public gjk_swig_wrapper {
    std::unique_ptr<algorithm::gjk> engine_;

public:
    gjk_connector();
    bool graze_with_one_dimensions(double y1, double x1, double y2, double x2) const override;
    bool collision_with_one_dimensions(double y1, double x1, double y2, double x2) const override;
};

#endif //GJK_CONNECTOR_H__
