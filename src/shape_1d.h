#ifndef SHAPE_1D_H__
#define SHAPE_1D_H__
#include "point.hpp"


namespace geometry {
class Shape1D { 
public:
    Shape1D(xy_t bounds);

    xy_t bounds() const;

private:
    xy_t bounds_;
};


}
#endif
