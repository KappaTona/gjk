#include "gjk_connector.h"
#include "euclidean_vector.h"
#include "gjk.h"
#include "shape_1d.h"
#include <utility>


gjk_swig_wrapper::~gjk_swig_wrapper() = default;
std::shared_ptr<gjk_swig_wrapper> gjk_swig_wrapper::create()
{
    return std::make_shared<gjk_connector>();
}

gjk_connector::gjk_connector() : engine_{std::make_unique<algorithm::gjk>()}
{
}

bool
gjk_connector::graze_with_one_dimensions(double y1, double x1, double y2, double x2) const
{
    return engine_->graze(std::make_pair(y1, x1), std::make_pair(y2, x2));
;
}

bool
gjk_connector::collision_with_one_dimensions(double y1, double x1, double y2, double x2) const 
{
    return engine_->collision(std::make_pair(y1, x1), std::make_pair(y2, x2));
}
