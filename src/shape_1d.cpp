#include "shape_1d.h"


namespace geometry {
Shape1D::Shape1D(xy_t bounds): bounds_{bounds}
{
    auto [x, y] = bounds_;
    if (x > y)
        bounds_ = std::make_pair(y, x);
}

xy_t Shape1D::bounds() const
{
    return bounds_;
}

}
