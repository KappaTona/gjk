#include <gtest/gtest.h>
#include <euclidean_vector.h>
#include <gjk.h>
#include <shape_1d.h>


using namespace geometry;
algorithm::gjk the_gjk;


TEST(test_gjk, collision_positive)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{1.1, 1.5}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, collision_negative)
{
    Shape1D s{{-2., -1.}};
    Shape1D o{{-1.1, -1.5}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, graze_positive)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{2.0, 3.5}};
    EXPECT_EQ(the_gjk.collision(s, o), false);
    EXPECT_EQ(the_gjk.graze(s, o), true);
}

TEST(test_gjk, graze_negative)
{
    Shape1D s{{-1., -2.}};
    Shape1D o{{-2.0, -3.5}};
    EXPECT_EQ(the_gjk.collision(s, o), false);
    EXPECT_EQ(the_gjk.graze(s, o), true);
}

TEST(test_gjk, not_collision_positive)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{2.1, 3.5}};
    EXPECT_EQ(the_gjk.collision(s, o), false);
}

TEST(test_gjk, not_collision_negative)
{
    Shape1D s{{-1., -2.}};
    Shape1D o{{-2.1, -3.5}};
    EXPECT_EQ(the_gjk.collision(s, o), false);
}

TEST(test_gjk, not_graze_positive)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{1.9, 2.5}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, not_graze_negative)
{
    Shape1D s{{-1., -2.3}};
    Shape1D o{{-3.4, -1.2}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, simplex_positive)
{
    Shape1D s{{1., 1.}};
    EXPECT_EQ(the_gjk.collision(s, s), false);
    EXPECT_EQ(the_gjk.graze(s, s), true);
}

TEST(test_gjk, simplex_negative)
{
    Shape1D s{{-1., -1.}};
    EXPECT_EQ(the_gjk.collision(s, s), false);
    EXPECT_EQ(the_gjk.graze(s, s), true);
}

TEST(test_gjk, collision_positive_reverse)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{1.1, 1.5}};
    EXPECT_EQ(the_gjk.collision(o, s), true);
    EXPECT_EQ(the_gjk.graze(o, s), false);
}

TEST(test_gjk, collision_negative_inverse)
{
    Shape1D s{{-1., -2.}};
    Shape1D o{{-1.5, -1.1}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, graze_positive_reverse)
{
    Shape1D s{{1., 2.}};
    Shape1D o{{2.0, 3.5}};
    EXPECT_EQ(the_gjk.collision(o, s), false);
    EXPECT_EQ(the_gjk.graze(o, s), true);
}

TEST(test_gjk, graze_negative_reverse)
{
    Shape1D s{{-1., -2.}};
    Shape1D o{{-2.0, -3.5}};
    EXPECT_EQ(the_gjk.collision(o, s), false);
    EXPECT_EQ(the_gjk.graze(o, s), true);
}

TEST(test_gjk, not_collision_positive_inverse)
{
    Shape1D s{{2., 1.}};
    Shape1D o{{3.5, 2.1}};
    EXPECT_EQ(the_gjk.collision(o, s), false);
}

TEST(test_gjk, not_collision_negative_inverse)
{
    Shape1D s{{-2., -1.}};
    Shape1D o{{-3.5, -2.1}};
    EXPECT_EQ(the_gjk.collision(s, o), false);
}

TEST(test_gjk, not_graze_positive_inverse)
{
    Shape1D s{{2., 1.}};
    Shape1D o{{2.5, 1.9}};
    EXPECT_EQ(the_gjk.collision(o, s), true);
    EXPECT_EQ(the_gjk.graze(o, s), false);
}

TEST(test_gjk, not_graze_negative_inverse)
{
    Shape1D s{{-2.3, -1.}};
    Shape1D o{{-1.2, -3.4}};
    EXPECT_EQ(the_gjk.collision(s, o), true);
    EXPECT_EQ(the_gjk.graze(s, o), false);
}

TEST(test_gjk, simplex_positive_inverse)
{
    Shape1D s{{1., 1.}};
    EXPECT_EQ(the_gjk.collision(s, s), false);
    EXPECT_EQ(the_gjk.graze(s, s), true);
}

TEST(test_gjk, simplex_negative_inverse)
{
    Shape1D s{{-1., -1.}};
    EXPECT_EQ(the_gjk.collision(s, s), false);
    EXPECT_EQ(the_gjk.graze(s, s), true);
}

TEST(test_gjk, is_convex)
{
    std::vector<geometry::EuclideanVector> convex = {
        {3., 1.},
        {1., 1.},
        {1., 3.},
        {3., 3.}
    };

    std::vector<geometry::EuclideanVector> big_convex = {
        {2., 8.},
        {2., 4.},
        {6., 4.},
        {8., 8.},
        {6., 10.},
    };
    std::vector<geometry::EuclideanVector> big_convex_shifted = {
        {6., 4.},
        {8., 8.},
        {6., 10.},
        {2., 8.},
        {2., 4.},
    };

    std::vector<geometry::EuclideanVector> big_convex_swap = {
        {8., 2.},
        {4., 2.},
        {4., 6.},
        {8., 8.},
        {10., 6.},
    };


    EXPECT_TRUE(the_gjk.is_convex(convex));
    EXPECT_TRUE(the_gjk.is_convex(big_convex));
    EXPECT_TRUE(the_gjk.is_convex(big_convex_shifted));
    EXPECT_TRUE(the_gjk.is_convex(big_convex_swap));
}

TEST(test_gjk, not_is_convex)
{
    std::vector<geometry::EuclideanVector> concav = {
        {3., 3.},
        {2., 0.},
        {0., 3.},
        {2., 2.7}
    };

    std::vector<geometry::EuclideanVector> big_concav = {
        {2., 4.},
        {6., 4.},
        {8., 8.},
        {6., 10.},
        {2., 8.},
        {3., 3.},
        {2., 0.},
        {0., 3.},
        {2., 2.7}
    };
    std::vector<geometry::EuclideanVector> big_concav_swap = {
        {4., 2.},
        {4., 6.},
        {8., 8.},
        {10., 6.},
        {8., 2.},
        {3., 3.},
        {0., 2.},
        {3., 0.},
        {2.7, 2.}
    };

    std::vector<geometry::EuclideanVector> big_concav_reverse = {
        {2.7, 2.},
        {3., 0.},
        {0., 2.},
        {3., 3.},
        {8., 2.},
        {10., 6.},
        {8., 8.},
        {4., 6.},
        {4., 2.}
    };

    EXPECT_FALSE(the_gjk.is_convex(concav));
    EXPECT_FALSE(the_gjk.is_convex(big_concav));
    EXPECT_FALSE(the_gjk.is_convex(big_concav_swap));
    EXPECT_FALSE(the_gjk.is_convex(big_concav_reverse));
}

TEST(test_gjk, collision_has_concave_polygon)
{
    std::vector<geometry::EuclideanVector> concav = {
        {3., 3.},
        {2., 0.},
        {0., 3.},
        {2., 2.7 }
    };

    std::vector<geometry::EuclideanVector> convex = {
        {3., 1.},
        {1., 1.},
        {1., 3.},
        {3., 3.}
    };

    EXPECT_ANY_THROW(the_gjk.collision(concav, convex));
}
