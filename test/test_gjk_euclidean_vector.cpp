#include <gtest/gtest.h>
#include <gjk.h>
#include <euclidean_vector.h>
#include <vector>


using namespace geometry;
algorithm::gjk the_gjk;


TEST(test_gjk, collision)
{
    std::vector<geometry::EuclideanVector> triangle_left = {
        {11, 4,},
        {5, 4,},
        { 9, 9 },
    };

    std::vector<geometry::EuclideanVector> triangle_right = {
        {7, 5},
        {3, 7},
        {2, 10},
        {7, 12},
    };

    bool col_true = the_gjk.collision(triangle_left, triangle_right);
    EXPECT_TRUE(col_true);
}

TEST(test_gjk, not_collision)
{
    std::vector<geometry::EuclideanVector> triangle_left = {
        { 0., 1. },
        { 3., 1.5 },
        { 0., 3. }
    };

    std::vector<geometry::EuclideanVector> triangle_right = {
        { 15, 15 },
        { 30., 30. },
        { 15, 45 }
    };

    bool col_false = the_gjk.collision(triangle_left, triangle_right);
    EXPECT_FALSE(col_false);
}

// special case need to read more about
TEST(test_gjk, same)
{
    std::vector<geometry::EuclideanVector> triangle = {
        { 0., 1. },
        { 3., 1.5 },
        { 0., 3. }
    };

    bool col_true = the_gjk.collision(triangle, triangle);
    EXPECT_TRUE(col_true);
}

TEST(test_gjk, big_convex_collision)
{
    std::vector<geometry::EuclideanVector> big_convex_left = {
        {2., 4.},
        {6., 4.},
        {8., 8.},
        {6., 10.},
        {2., 8.},
    };

    std::vector<geometry::EuclideanVector> big_convex_right = {
        {3., 5.},
        {7., 5.},
        {9., 9.},
        {7., 11.},
        {3., 9.},
    };

    bool col_true = the_gjk.collision(big_convex_left, big_convex_right);
    EXPECT_TRUE(col_true);
}
