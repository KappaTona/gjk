#include <gtest/gtest.h>
#include <ostream>
#include <euclidean_vector.h>


using namespace geometry;


TEST(test_euclidean_vector, properties)
{
    EuclideanVector xd{1., 0.};

    EXPECT_DOUBLE_EQ(xd.x(), 0.);
    EXPECT_DOUBLE_EQ(xd.y(), 1.);

    xd.x(2.);
    xd.y(3.);

    EXPECT_DOUBLE_EQ(xd.x(), 2.);
    EXPECT_DOUBLE_EQ(xd.y(), 3.);

}

TEST(test_euclidean_vector, subtract)
{
    EuclideanVector one{1., 2.};
    EuclideanVector two{2., 3.};
    EuclideanVector expected{1., 1.};
    auto sub = two - one;

    EXPECT_TRUE(sub == expected);
}

TEST(test_euclidean_vector, subtract_in_place)
{
    EuclideanVector one{1., 1.};
    EuclideanVector two{2., 3.};
    one -= two;
    EuclideanVector expected{-1., -2.};

    EXPECT_TRUE(one == expected);
}


TEST(test_euclidean_vector, add)
{
    EuclideanVector one{1., 2.};
    EuclideanVector two{2., 3.};
    EuclideanVector expected{3., 5.};
    auto add = two + one;

    EXPECT_TRUE(add == expected);
}

TEST(test_euclidean_vector, add_in_place)
{
    EuclideanVector one{1., 1.};
    EuclideanVector two{2., 3.};
    one += two;
    EuclideanVector expected{3., 4.};

    EXPECT_TRUE(one == expected);

}

TEST(test_euclidean_vector, negate)
{
    EuclideanVector neg{1.5, 1.5};
    EuclideanVector expected{-1.5, -1.5};
    auto rv = !neg;

    EXPECT_TRUE(rv == expected);
    EXPECT_DOUBLE_EQ(neg.y(), 1.5);
    EXPECT_DOUBLE_EQ(neg.x(), 1.5);
}

TEST(test_euclidean_vector, scale_up)
{
    EuclideanVector mult{9.5, 7.7};
    mult *= 1.5;
    EuclideanVector bot{14.25, 11.55};

    EXPECT_TRUE(bot >= mult);
}

TEST(test_euclidean_vector, scale_down)
{
    EuclideanVector div{9.5, 7.7};
    div /= 1.5;
    EuclideanVector top{6.3, 5.13};

    EXPECT_TRUE(top <= div);
}

TEST(test_euclidean_vector, perpendicular)
{
    EuclideanVector p{1., 1};
    EuclideanVector expected{-1, 1.};
    EuclideanVector p2{0., 1};
    EuclideanVector expected2{-1, 0.};

    EXPECT_TRUE(p.perpendicular() == expected);
    EXPECT_TRUE(p2.T() == expected2);
}

TEST(test_euclidean_vector, dot_product)
{
    EuclideanVector one{7.7, 8.8};
    EuclideanVector other{7.7, 8.8};
    double expected = 7.7 * 7.7 + 8.8 * 8.8;

    EXPECT_DOUBLE_EQ(expected, one.dot(other));
}

TEST(test_euclidean_vector, length_squared)
{
    EuclideanVector vec{7.7, 9.9};
    double expected = 157.3;

    EXPECT_TRUE(vec.length_squared() == vec.size());
    EXPECT_DOUBLE_EQ(vec.length_squared(), expected);
}

TEST(test_euclidean_vector, triple_dot)
{
    EuclideanVector a{-4.4, -1.9};
    EuclideanVector c{-4.4, -1.9};

    EXPECT_DOUBLE_EQ(a.dot(c), 22.97);
    EXPECT_DOUBLE_EQ(a.y(), -4.4);
    EXPECT_DOUBLE_EQ(a.x(), -1.9);
    EXPECT_DOUBLE_EQ(c.y(), -4.4);
    EXPECT_DOUBLE_EQ(c.x(), -1.9);
}
