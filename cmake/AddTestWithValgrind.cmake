function(my_add_test _name)
    add_executable(${_name} ${_name}.cpp)
    add_test(NAME ${_name} COMMAND ${_name})
    target_link_libraries(${_name} gjk gtest gtest_main)
    if(VALGRIND)
        set(_LOG_FILE --log-file=${CMAKE_CURRENT_BINARY_DIR}/${_name}_valgrind.log)
        add_custom_command(TARGET ${_name} POST_BUILD
            COMMAND ${MEMORYCHECK_COMMAND} ${MEMORYCHECK_COMMAND_OPTIONS} ${_LOG_FILE}
                ${CMAKE_CURRENT_BINARY_DIR}/${_name}
            COMMENT "-- ${_name}_valgrind.log will be generated to ${CMAKE_CURRENT_BINARY_DIR}/${_name}_valgrind.log"
            DEPENDS ${_name}
        )
    endif()
endfunction()
