#include <exception>
#include <iostream>
#include <ostream>
#include <vector>
#include "gjk.h"
#include "euclidean_vector.h"
#include "shape_1d.h"


void gjk_1d_i_suppose()
{
    geometry::Shape1D one{{10., 20.}};
    geometry::Shape1D another{{1., 10.}};

    algorithm::gjk ni;
    auto [yesorno, we] = ni.boom(another, one);
    std::cout << we << std::endl;
}

int main()
{
    std::vector<geometry::EuclideanVector> triangle_left = {
        {0., 1.},
        {3., 1.6},
        {0., 3.}
    };

    std::vector<geometry::EuclideanVector> triangle_right = {
        {1.6, 1.7},
        {3., 3.},
        {1.5, 3.5},
    };

    algorithm::gjk g;
    bool col_true = g.collision(triangle_left, triangle_right);
    std::cout << (col_true ? "true" : "false") << std::endl;

    std::vector<geometry::EuclideanVector> convex = {
        {3., 1.},
        {1., 1.},
        {1., 3.},
        {3., 3.}
    };

    std::vector<geometry::EuclideanVector> concav = {
        {3., 3.},
        {2., 0.},
        {0., 3.},
        {2., 2.7 }
    };

    bool k = g.is_convex(convex);
    std::cout << "1st:\t" << std::flush;
    std::cout << (k ? "convex" : "wrong") << std::endl;
    k = g.is_convex(concav);
    std::cout << "2nd:\t" << std::flush;
    std::cout << (k ? "wrong" : "concav") << std::endl;

    std::vector<geometry::EuclideanVector> big_convex = {
        {2., 8.},
        {2., 4.},
        {6., 4.},
        {8., 8.},
        {6., 10.},
    };
    std::vector<geometry::EuclideanVector> big_convex2 = {
        {6., 4.},
        {8., 8.},
        {6., 10.},
        {2., 8.},
        {2., 4.},
    };

    std::vector<geometry::EuclideanVector> big_convex3 = {
        {2., 4.},
        {6., 4.},
        {8., 8.},
        {6., 10.},
        {2., 8.},
    };


    k = g.is_convex(big_convex);
    std::cout << "3rd:\t" << std::flush;
    std::cout << (k ? "convex" : "wrong") << std::endl;
    k = g.is_convex(big_convex2);
    std::cout << "4th:\t" << std::flush;
    std::cout << (k ? "convex" : "wrong") << std::endl;
    k = g.is_convex(big_convex3);
    std::cout << "5th:\t" << std::flush;
    std::cout << (k ? "convex" : "wrong") << std::endl;

    try
    {
        g.collision(concav, convex);
    } catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
 
    return 0;
}
