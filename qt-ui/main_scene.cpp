#include "main_scene.h"
#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <QPixmap>
#include <QPushButton>
#include <QResource>


namespace {
QPushButton* make_code_button(QString text, int code)
{
    auto* code_button = new QPushButton(text);
    code_button->setSizePolicy({QSizePolicy::MinimumExpanding, QSizePolicy::Minimum});
    QApplication::connect(code_button, &QPushButton::clicked, [code] { qApp->exit(code);} );
    return code_button;
}
}

namespace ui {
MainScene::MainScene(QWidget* parent) : QWidget{parent}
{
    QResource::registerResource("res/resource.qrc");
    auto* grid = new QGridLayout;
    auto* buttons = new QHBoxLayout;
    buttons->addWidget(make_code_button("&Exit",0));
    buttons->addWidget(make_code_button("&Restart", 888));
    auto* placeholder = new QLabel;
    placeholder->setPixmap(QPixmap::fromImage(QImage{":/default"}));

    grid->addWidget(placeholder, 1, 0);
    grid->addLayout(buttons, 2, 0);
    setLayout(grid);
    show();
}
}
