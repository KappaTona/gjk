#ifndef VIEW_H__
#define VIEW_H__
#include <QDialog>


namespace ui {
class View : public QDialog
{
    Q_OBJECT
    QWidget* main_widget_;
public:
    View(int width, int height);
    void restart();
    void start();
};

}

#endif //VIEW_H__
