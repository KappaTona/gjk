#include "view.h"
#include "main_scene.h"

namespace ui {
View::View(int width, int height)
{
    setFixedWidth(width);
    setFixedHeight(height);
    start();
    show();
}

void View::restart()
{
    delete main_widget_;
    start();
}

void View::start()
{
    main_widget_ = new MainScene{this};
}


}
