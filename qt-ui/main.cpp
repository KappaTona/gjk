#include <QApplication>
#include <memory>
#include <utility>
#include "view.h"


int main(int argc, char** argv)
{
    auto app = std::make_unique<QApplication>(argc, argv);
    auto dialog = std::make_unique<ui::View>(800, 600);

    auto exit_code = app->exec();
    while (exit_code)
    {
        exit_code = app->exec();
        dialog->restart();
    }
    return 0;
}
