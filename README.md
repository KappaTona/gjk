A good inspiration has emerged from [here](https://github.com/kroitor/gjk.c.git)
Details: [gjk-wiki](https://en.wikipedia.org/wiki/Gilbert%E2%80%93Johnson%E2%80%93Keerthi_distance_algorithm)

TODO:

- 0! GJK doesn't know about "shape" refactor later..
- 0 1D visual
- 0 2D same as 1D (without visual for now)
- 0 3D same as 1D (without visual for now)
- 0 4D same as 1D (without visual for now)
- 0 Artifact -> basically an .so for Linux
- 0 python + swig + robot-framework


Notes:
- `cmake -DBUILD_TEST=ON -DVALGRIND=ON`
- for now:
    - cd into `${CMAKE_BINARY_DIR}/test/test_gjk && CTEST_OUTPUT_ON_FAILURE=1 ctest`
        - now this runs automatically if `VALGRIND=ON`
